/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use <../../Archetype/OpenSCAD/assembly_filled.scad>

scale = 1 / 87;

/**
 * This is an H0 implementation of the DB Wagon Door Buffer.
 *
 * Filled, as one piece.
 */
module door_buffer_filled() {
  scale([scale, scale, scale]) assembly_filled();
}

door_buffer_filled($fn = 200);
