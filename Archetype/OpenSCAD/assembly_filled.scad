/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use <housing.scad>
use <housing_inner_space.scad>
use <console.scad>
use <bolt.scad>
use <cover_plate.scad>

/**
 * A door buffer for covered goods wagon class Gs 216 of Deutsche Bundesbahn.
 *
 * All parts assembled and filled.
 */
module assembly_filled() {

  length = housing_dimensions()[0];
  width = housing_dimensions()[1];
  height = housing_dimensions()[2];
  material_thickness = housing_dimensions()[3];

  translate([25, 5, 0]) housing(length, width, height, material_thickness);
  color("blue") translate([25, 5, 0]) housing_inner_space(length, width, height, material_thickness);
  console();
  translate([130, 80, 40]) rotate([0, -90, 0]) bolt();
  translate([25, 10, 75]) cover_plate(265, 100, 5);
}

assembly_filled($fn = 100);
