/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Cover plate of the door buffer
 *
 * Sources:
 * [1] Pictures by JR
 */
module cover_plate(length, width, material_thickness) {
  gap = 0.5; // To let fit in
  inner_length = length - width / 2;
  radius = width / 2;

  salient_length = 30; // Taken from [1]
  salient_width = 10; // Taken from [1]

  _plate();
  _salient();

  module _plate() {
    linear_extrude(material_thickness) union() {
      // Add the gap
      offset(r = -gap) {
        square([inner_length, width]);
        translate([inner_length, radius]) circle(r = radius);
      }
      // But we do not need the gap at the left side (there is no boundary)
      translate([0, gap]) square([gap, width - 2 * gap]);
    }
  }

  /**
   * "Erhebung"
   */
  module _salient() {
    thickness = 3; // Taken from [1]
    distance = 15; // Taken from [1]

    translate([distance, width / 2 - salient_length / 2, material_thickness]) cube([salient_width, salient_length, thickness]);
  }
}

cover_plate(265, 100, 5);
