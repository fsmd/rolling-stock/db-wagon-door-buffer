/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A door buffer for covered goods wagon class Gs 216 and
 * other of Deutsche Bundesbahn.
 *
 * Sources:
 * [1] Photos by JR
 */
module housing(length, width, height, material_thickness) {

  translate([length - width / 2, width / 2, height / 2]) _housing();

  function get_dimensions() = [length, width, height, material_thickness];

  module _housing() {
    union() {
      difference() {
        union() {
          cylinder(h = height, d = width, center = true);
          translate([-(length - width / 2) / 2, 0, 0]) cube([length - width / 2, width, height], true);
        }
        cylinder(h = height * 1.1, d = width - 2 * material_thickness,  center = true);
        translate([-(length - width / 2) / 2 * 1.1, 0, 0]) cube([(length - width / 2) * 1.1, width - 2 * material_thickness, height * 1.1], true);
        // Lower holes
        hole_diameter = 15;
        distance = 75;
        for (i = [0:2]) {
          translate([i * distance, 0, 0]) translate([-length + width / 2 + hole_diameter + 20, -width / 2 + material_thickness / 2, 0]) rotate([90, 0, 0]) cylinder(h = material_thickness * 1.1, d = hole_diameter, center = true);
        }
      }
      difference() {
        translate([-(length - width / 2 - material_thickness / 2), 0, -material_thickness / 2]) cube([material_thickness, width, height - material_thickness], true);
        translate([-(length - width / 2) - material_thickness * 0.1 / 2, +20, 0]) rotate([0, 90, 0]) cylinder(d = 40, h = material_thickness * 1.1);
      }
    }
  }
}

function housing_dimensions() = [length, width, height, material_thickness];

length = 270;
width = 110;
height = 80;
material_thickness = 5;

housing(length, width, height, material_thickness, $fn = 100);
