/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use <housing.scad>

/**
 * Describes the inner space of the housing.
 *
 *
 */
module housing_inner_space(length, width, height, material_thickness) {

  inner_length = length - width / 2;
  radius = width / 2;

  _inner_space();

  module _inner_space() {
    linear_extrude(height - material_thickness) union() {
      // Remove the material thickness
      offset(r = -material_thickness) {
        square([inner_length, width]);
        translate([inner_length, radius]) circle(r = radius);
      }
    }
  }
}

length = housing_dimensions()[0];
width = housing_dimensions()[1];
height = housing_dimensions()[2];
material_thickness = housing_dimensions()[3];

housing_inner_space(length, width, height, material_thickness, $fn = 100);
