/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A door buffer for covered goods wagon class Gs 216 and
 * other of Deutsche Bundesbahn.
 *
 * Sources:
 * [1] Photos by JR
 */
module console() {

  length = 25 + 270 + 5; // Taken from [1]
  width = 120; // Taken from [1]
  thickness = 5; // Taken from [1]

  translate([0, 0, -thickness]) cube([length, width, thickness]);
}

console();
