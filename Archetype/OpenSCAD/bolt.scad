/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The bold - simplified!
 *
 * Sources:
 * [1]: Photos taken from JR
 * [2]: Güterwagen-Ausschuss-Protokoll
 */
module bolt() {
  diameter = 35; // = 2a -- Taken from [1]
  length = 150; //
  outer_length = 50;

  flattening_length = 15; // Measured from [2]
  flattening_depth = 2.5; // Measured from [2]

  // Spherical segment
  h = 2.5; // Taken from [1]
  a = diameter / 2;
  // 2rh = a^2 + h^2
  // r = (a^2 + h^2) / 2h
  r = (a * a + h * h) / (2 * h);

  difference() {
    union() {
      intersection() {
        translate([0, 0, -r + length + h]) sphere(r = r);
        cylinder(h = length + h, d = diameter);
      }
      cylinder(h = length, d = diameter);
      translate([0, 0, length - outer_length]) _disk();
    }
    _flattening();
    mirror([0, 1, 0]) _flattening();
  }

  module _flattening() {
    translate([0, diameter / 2 -  flattening_depth / 2, -flattening_length / 2 + length]) cube([diameter, flattening_depth, flattening_length + h], true);
  }

  module _disk() {
    thickness = 4;
    diameter = 50;
    translate([0, 0, -thickness]) cylinder(h = thickness, d = diameter);
  }
}

bolt($fn = 100);
